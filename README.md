# Voxl-libyaml

This is a build of the [libyaml](https://github.com/yaml/libyaml) library for voxl, aka the YAML C library. 
This library is referenced to the [offical YAML repository.](https://yaml.org/) using the "C" Fast YAML 1.1 [implementation](https://github.com/yaml/libyaml).


## Build Instructions

1) prerequisite: the voxl-cross docker image

Follow the instructions here to build and install the voxl-cross docker image:

https://gitlab.com/voxl-public/voxl-docker

2) Launch the voxl-cross docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/libyaml$ voxl-docker -i voxl-cross
voxl-cross:~/voxl-libyaml$
voxl-cross:~/voxl-libyaml$ ls
build.sh  CHANGELOG  clean.sh  CMakeLists.txt  install_on_voxl.sh  ipk  libyaml  LICENSE  make_package.sh  README.md
voxl-cross:~/voxl-libyaml$
```

4) The build script takes one of these 4 options to set the architecture this should build for:

* `./build.sh native` Builds using whatever native gcc lives at /usr/bin/gcc
* `./build.sh 32`     Builds using the 32-bit (armv7) arm-linux-gnueabi cross-compiler that's in the voxl-cross docker
* `./build.sh 64`     Builds using the 64-bit (armv8) aarch64-gnu cross-compiler that's in the voxl-cross docker
* `./build.sh both`   Builds both 32 and 64 cross-compiled binaries. This is what's used to build the VOXL IPK packages

```bash
voxl-cross:~/libyaml$ ./build.sh both
```

5) Make an ipk package while still inside the docker.

```bash
voxl-cross:~/libyaml$ ./make_package.sh

Package Name:  libyaml
version Number:  x.x.x
...
ar: creating libyaml_x.x.x.ipk

DONE
```

This will make a new libyaml_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.

It will install whatever was built in the previous step based on your chosen compiler. For the "both" option it will install 32 and 64 bit versions of the lib, one copy of the headers, and just the 64-bit example programs.


## Deploy to VOXL

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/libyaml$ ./install_on_voxl.sh
pushing libyaml_x.x.x.ipk to target
searching for ADB device
adb device found
libyaml_x.x.x.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package libyaml from root...
Installing libyaml (x.x.x) on root.
Configuring libyaml

Done installing libyaml
```

